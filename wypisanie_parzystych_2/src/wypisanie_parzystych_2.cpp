#include <iostream>
using namespace std;

int main() { //wypisuje liczby parzyste z zakresu sposob 2
	cout << "Witaj w programie wypisujacym liczby parzyste z podanego zakresu!"
			<< endl;
	cout << "Podaj zakres liczb:" << endl;
	cout << "a = ";
	int poczatekZakresu;
	cin >> poczatekZakresu;
	cout << "b = ";
	int koniecZakresu;
	cin >> koniecZakresu;

	if (poczatekZakresu > koniecZakresu) {
		int temp;
		temp = poczatekZakresu;
		poczatekZakresu = koniecZakresu;
		koniecZakresu = temp;
	}
	for (int i = poczatekZakresu; i <= koniecZakresu; ++i) {
		if (i % 2 == 0)
			cout << i << " ";
	}
	return 0;
}

#include <iostream>
using namespace std;
