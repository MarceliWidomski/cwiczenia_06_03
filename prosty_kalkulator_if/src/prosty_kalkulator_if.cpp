// kalkulator na if
#include <iostream>
using namespace std;

int main() {
	int opcja;
	char znakOperacji;
	double pierwszaLiczba;
	double drugaLiczba;
	double wynik;
	do {
		cout << "Menu:" << endl;
		cout << "Wybierz numer opcji: " << endl;
		do {
			cout << "1) Wykonaj dzia�anie na dwoch liczbach" << endl;
			cout << "2) Wylacz kalkulator" << endl;
			cin >> opcja;
			if (opcja != 1 && opcja != 2) {
				cout << "Wybrano niewlasciwa opcje, wybierz ponownie" << endl;
			}
		} while (opcja != 1 && opcja != 2);

		if (opcja == 1) {
			cout << "Podaj znak operacji, ktora chcialbys wykonac (+, -, *, /): ";
			cin >> znakOperacji;

			cout << "Podaj pierwsza liczbe: ";
			cin >> pierwszaLiczba;

			cout << "Podaj druga liczbe: ";
			cin >> drugaLiczba;

			if (znakOperacji == '+') {
				wynik = pierwszaLiczba + drugaLiczba;
			} else if (znakOperacji == '-') {
				wynik = pierwszaLiczba - drugaLiczba;
			} else if (znakOperacji == '*') {
				wynik = pierwszaLiczba * drugaLiczba;
			} else if (znakOperacji == '/') {
				wynik = pierwszaLiczba / drugaLiczba;
			}
			else{
				cout << "Niewlasciwy znak operacji. Wylaczam..." << endl;
			return 0;
			}

			cout << pierwszaLiczba << znakOperacji << drugaLiczba << "=" << wynik << endl;
			opcja = 0;
		} else {
			cout << "Wylaczam..." << endl;
		}
	} while (opcja != 1 && opcja != 2);
	return 0;
}
