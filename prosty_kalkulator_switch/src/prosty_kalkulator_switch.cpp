//kalkulator na switch

#include <iostream>
using namespace std;

int main() {
	int opcja;
	char znakOperacji;
	double pierwszaLiczba;
	double drugaLiczba;
	double wynik;

	do {
		cout << "Menu:" << endl;
		cout << "Wybierz numer opcji: " << endl;
		cout << "1) Wykonaj dzia�anie na dwoch liczbach" << endl;
		cout << "2) Wylacz kalkulator" << endl;
		cin >> opcja;

		switch (opcja) {
		case 1: {
			cout
					<< "Podaj znak operacji, ktora chcialbys wykonac (+, -, *, /): ";
			cin >> znakOperacji;
			cout << "Podaj pierwsza liczbe: ";
			cin >> pierwszaLiczba;
			cout << "Podaj druga liczbe: ";
			cin >> drugaLiczba;
			switch (znakOperacji) {
			case '+':
				wynik = pierwszaLiczba + drugaLiczba;
				break;
			case '-':
				wynik = pierwszaLiczba - drugaLiczba;
				break;
			case '*':
				wynik = pierwszaLiczba * drugaLiczba;
				break;
			case '/':
				wynik = pierwszaLiczba / drugaLiczba;
				break;
			default: {
				cout << "Niewlasciwy znak operacji. Wylaczam..." << endl;
				return 0;
			}

			}
			cout << pierwszaLiczba << znakOperacji << drugaLiczba << "="
					<< wynik << endl;
			opcja = 0;
			break;
		}

		case 2: {
			cout << "Wylaczam..." << endl;
			break;
		}

		default:
			cout << "Wybrano niewlasciwa opcje, wybierz ponownie" << endl;
		}
	} while (opcja != 1 && opcja != 2);
	return 0;
}

