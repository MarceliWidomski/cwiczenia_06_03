#include <iostream>
using namespace std;

int main() { //wypisuje liczby parzyste z zakresu
	cout << "Witaj w programie wypisujacym liczby parzyste z podanego zakresu!" << endl;
	cout << "Podaj zakres liczb:" << endl;
	cout << "a = ";
	int poczatekZakresu;
	cin >> poczatekZakresu;
	cout << "b = ";
	int koniecZakresu;
	cin >> koniecZakresu;

	if (poczatekZakresu>koniecZakresu){ //jesli uzytkownik podal liczby w zlej kolejnosci zamienia je miejscami
		int temp;
		temp=poczatekZakresu;
		poczatekZakresu=koniecZakresu;
		koniecZakresu=temp;
	}
	for (int i = poczatekZakresu; i <= koniecZakresu; i = i + 2) {
		if (poczatekZakresu % 2 == 0)
			cout << i << " ";
		else
			cout << i + 1 << " ";
	}
	return 0;
}
