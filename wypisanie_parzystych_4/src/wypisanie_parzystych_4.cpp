#include <iostream>
using namespace std;

int main() { // sprawdza czy liczba jest parzysta poprzez iloczyn bitowy
	cout << "Witaj w programie wypisujacym liczby parzyste z podanego zakresu!"
			<< endl;
	cout << "Podaj zakres liczb:" << endl;
	cout << "a = ";
	int poczatekZakresu;
	cin >> poczatekZakresu;
	cout << "b = ";
	int koniecZakresu;
	cin >> koniecZakresu;

	if (poczatekZakresu > koniecZakresu) {
		int temp;
		temp = poczatekZakresu;
		poczatekZakresu = koniecZakresu;
		koniecZakresu = temp;
	}
	for (int i = poczatekZakresu; i <= koniecZakresu; ++i) {
		if ((i & 1) == 0)
			//continue;
			cout << i << " ";
	}
	return 0;
}
