#include <iostream>
using namespace std;

int main() {//wypisje liczby podzielne przez 6 z przedzialu
	cout << "Witaj w programie wypisujacym liczby podzielne przez 6 z danego zakresu"
			<< endl;
	cout << "Podaj zakres liczb:" << endl;
	cout << "a = ";
	int poczatekZakresu;
	cin >> poczatekZakresu;
	cout << "b = ";
	int koniecZakresu;
	cin >> koniecZakresu;

	if (poczatekZakresu > koniecZakresu) {
		int temp;
		temp = poczatekZakresu;
		poczatekZakresu = koniecZakresu;
		koniecZakresu = temp;
	}
	for (int i = poczatekZakresu; i <= koniecZakresu; ++i) {
		if (i % 2 == 0 && i % 3 == 0) // liczba jest podzielna przez 6 jesli jest podzielna przez 2 i 3
		cout << i << " ";
	}
	return 0;
}
